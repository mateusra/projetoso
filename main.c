#include "main.h"

sem_t mutex, full, notfull;
short file_to_write = 0;
short file_to_read = 0;

char* product_something(int id){
    srand(time(NULL));
	char* resp = (char*) malloc(sizeof(char)*FILE_SIZE);
	int i = 0;
	sprintf(resp,"Isso foi produzido pelo produtor %d\n", id);
	clock_t good_clock;
	for(i=strlen(resp); i < FILE_SIZE-1; i++){
		 good_clock = clock();
		 resp[i] = ':';
	}
	resp[FILE_SIZE-1]='\0';
	return resp;
}

void insert_something(int id, char *something) {
	FILE *f;
	char fileName[FILE_NAME];
	sprintf(fileName, "file-%d.txt", file_to_write);
	f = fopen(fileName,"w");
	fprintf(f, "%s", something);
	fclose(f);
}

void consume_something(int id){
    FILE *f;
	char fileName[FILE_NAME];
	sprintf(fileName, "file-%d.txt", file_to_read);
	f = fopen(fileName,"r");
	char data[FILE_SIZE];
	fscanf(f, "%s", data);
	fclose(f);
}

void* producer(void * id){
    int i = 0;
	char* something;
	while(1){
		something = product_something(*((int*)id));
		sem_wait(&notfull);
		sem_wait(&mutex);
		insert_something(*((int*)id), something);
		file_to_write = (file_to_write + 1) % BUFFER_SIZE;
		sem_post(&mutex);
		sem_post(&full);
	}
    return NULL;

}

void* consumer(void * id){
	while(1){
	    sem_wait(&full);
		sem_wait(&mutex);
		consume_something(*((int*)id));
    		file_to_read = (file_to_read + 1) % BUFFER_SIZE;
		sem_post(&mutex);
		sem_post(&notfull);
	}
    return NULL;
} 

int main(void){
    sem_init(&mutex, 1, 1);
    sem_init(&full, 1, 0);
    sem_init(&notfull, 1, BUFFER_SIZE);
    pthread_t producers[PRODUCERS];
	pthread_t consumers[CONSUMERS];
	int i = 0;
	int producers_id[PRODUCERS];
	for (i = 0; i < PRODUCERS; i++) {
	    producers_id[i] = i+1;
	    pthread_create(&producers[i], NULL, producer, &producers_id[i]);
	}
	int consumers_id[CONSUMERS];
	for (i = 0; i < CONSUMERS; i++) {
	    consumers_id[i] = i+1;
	    pthread_create(&consumers[i], NULL, consumer,&consumers_id[i]);
	}
	
	
	for (i = 0; i < PRODUCERS; i++) {
	    pthread_join(producers[i], NULL);
	}
	for (i = 0; i < CONSUMERS; i++) {
	    pthread_join(consumers[i], NULL);
	}
	return 0;


}
